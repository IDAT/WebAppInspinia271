<%@page import="BusinessEntity.DivisionBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="BusinessLogic.DivisionController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    DivisionController c = new DivisionController();
    ArrayList<DivisionBean> lst = c.reaDivision();    

%>
<!DOCTYPE html>
<html>
<!-- Style CSS -->
<%@include file="include_style.jsp" %>

<body class="">

    <div id="wrapper">
        <!-- NavBar -->
        <%@include file="include_navbar.jsp" %>    

        <div id="page-wrapper" class="gray-bg">
            <!-- NavBarHeader -->
            <%-- --%>
            <%@include file="include_navbarheader.jsp" %>            
            <%-- --%>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Division</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li class="active">
                            <strong>Division</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="upd_division.jsp" class="btn btn-primary">Nuevo</a>
                        <a href="" class="btn btn-primary">Botónes</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">              
                   
                    <!-- -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Mantenimiento Division</h5>
                                </div>
                                <div class="ibox-content">
                                    <!-- TABLAS FORMS IU -->
                                    <table class="footable table table-stripped toggle-arrow-tiny" data-filter=#top-search>
                                        <thead>
                                            <th>Codigo</th>
                                            <th>UniCod</th>
                                            <th>Unidad</th>
                                            <th>Division</th>
                                            <th>Estado</th>
                                            <th>Acciones</th>
                                        </thead>
                                        <tbody>
                                            <%
                                                for(int x=0; x < lst.size(); x++){
                                                    DivisionBean i = lst.get(x); //item                                                
                                            %>
                                            <tr>
                                                <td><%=i.getDiv_codigo() %></td>
                                                <td><%=i.getDiv_unicod() %></td>
                                                <td><%=i.getUni_descri() %></td>
                                                <td><%=i.getDiv_descri() %></td>
                                                <td><%=(i.getDiv_estcod() == 0 ? "INACTIVO" : "ACTIVO") %></td>
                                                <td>
                                                    <a href="upd_division.jsp?token=<%=i.getDiv_codigo() %>" class="btn btn-primary btn-xs">
                                                        <i class="fa fa-pencil"></i>
                                                        Editar</a>
                                                    <a href="upd_division.jsp?token=<%=i.getDiv_codigo() %>" class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash"></i>
                                                        Eliminar</a>
                                                </td>
                                            </tr>
                                            <%
                                            }
                                            %>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="6">
                                                    <ul class="pagination pull-right"></ul>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- -->
           
            </div>
            <!-- Footer -->
            <%--
            <%@include file="include_footer.jsp" %>
            --%>
        </div>
    </div>
    <!-- Script -->
    <%@include file="include_script.jsp" %>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {
            $('.footable').footable();
        });
    </script>
</body>
</html>
