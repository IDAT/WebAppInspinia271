<%@page import="java.util.ArrayList"%>
<%@page import="BusinessEntity.UsuarioModel"%>
<%@page import="BusinessLogic.UsuarioController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    UsuarioController UBL = new UsuarioController();
    //EVALUACION DE EXISTEN INICIAL DE PARAMETROS
    if(request.getParameter("accion") != null){        
        //ACCION -> ELIMINAR
        if("DELETE".equalsIgnoreCase(request.getParameter("accion"))  && request.getParameter("token") != null ){            
            //TOKEN -> CODIGO USUARIO
            UBL.deleteUsuario(request.getParameter("token"));
            response.sendRedirect("rea_usuarios.jsp");
        }
        if("UPDATE".equalsIgnoreCase(request.getParameter("accion"))){
            UsuarioModel UBE = new UsuarioModel();
            UBE.setUsu_codigo(request.getParameter("txtCodigo"));
            UBE.setUsu_nombre(request.getParameter("txtNombre"));
            UBE.setUsu_descri(request.getParameter("txtDescri"));
            UBE.setUsu_passwd(request.getParameter("txtPasswd"));
            UBE.setUsu_email(request.getParameter("txtEmail"));
            UBE.setUsu_imagen("-");
            UBE.setUsu_estcod(Integer.valueOf(request.getParameter("cboEstado")));
            UBL.updateUsuario(UBE);
        }
        if("CREATE".equalsIgnoreCase(request.getParameter("accion"))){
            UsuarioModel UBE = new UsuarioModel();
            UBE.setUsu_codigo(request.getParameter("txtCodigo"));
            UBE.setUsu_nombre(request.getParameter("txtNombre"));
            UBE.setUsu_descri(request.getParameter("txtDescri"));
            UBE.setUsu_passwd(request.getParameter("txtPasswd"));
            UBE.setUsu_email(request.getParameter("txtEmail"));
            UBE.setUsu_imagen("-");
            UBE.setUsu_estcod(Integer.valueOf(request.getParameter("cboEstado")));
            UBL.createUsuario(UBE);
        }
    }
    
    ArrayList<UsuarioModel> aryUBE = UBL.readUsuario();
%>
<!DOCTYPE html>
<html>
    <%@include file="include_style.jsp" %>
<body>

    <div id="wrapper">
        <!-- NavBar -->
        <%@include file="include_navbar.jsp" %>    

        <div id="page-wrapper" class="gray-bg">
            <!-- NavBarHeader -->
            <%-- --%>
            <%@include file="include_navbarheader.jsp" %> 
            
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Usuarios</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Mantenimiento</a>
                        </li>
                        <li class="active">
                            <strong>Usuarios</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="upd_usuarios.jsp?accion=CREATE"><button class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Nuevo</button></a>
                        <a href="rea_usuarios.jsp"><button class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Actualizar</button></a>
                        <button class="btn btn-sm btn-default"><i class="fa fa-print"></i> Imprimir</button>
                        <button class="btn btn-sm btn-default"><i class="fa fa-file-pdf-o"></i> Exportar</button>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Mantenimiento de Usuario</h5>
                        </div>
                        <div class="ibox-content">                            
                            <table class="footable table table-stripped toggle-arrow-tiny" data-filter=#top-search>
                                <thead>
                                <tr>
                                    <th data-toggle="true" data-type="numeric">Codigo</th>
                                    <th data-sort-initial="true" data-hide="all">Nombre</th>
                                    <th >Descri</th>
                                    <th data-hide="all">Contraseña</th>
                                    <th data-hide="all">Correo</th>
                                    <th data-hide="all">Foto</th>
                                    <th data-sort-ignore="true">Estado</th>
                                    <th data-sort-ignore="true">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <% for(int x=0; x < aryUBE.size(); x++){%>
                                <tr>                                   
                                    <td><%=aryUBE.get(x).getUsu_codigo() %></td>
                                    <td><%=aryUBE.get(x).getUsu_nombre() %></td>
                                    <td><%=aryUBE.get(x).getUsu_descri() %></td>
                                    <td><%=aryUBE.get(x).getUsu_passwd() %></td>
                                    <td><%=aryUBE.get(x).getUsu_email() %></td>
                                    <td><%=aryUBE.get(x).getUsu_imagen() %></td>
                                    <td><%=(aryUBE.get(x).getUsu_estcod()== 0 ? "INACTIVO" : "ACTIVO") %></td>
                                    <td>
                                        <a href="upd_usuarios.jsp?accion=UPDATE&token=<%=aryUBE.get(x).getUsu_codigo() %>" class="btn btn-primary btn-xs">
                                                        <i class="fa fa-pencil"></i>
                                                        Editar</a>
                                        &nbsp;
                                        <a href="rea_usuarios.jsp?accion=DELETE&token=<%=aryUBE.get(x).getUsu_codigo() %>" class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash"></i>
                                                        Eliminar</a>
                                    </td>
                                </tr>
                                <%}%>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            <ul class="pagination pull-right"></ul>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
            <%@include file="include_footer.jsp" %>

        </div>
        </div>

   <%@include file="include_script.jsp" %>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {
            $('.footable').footable();
        });
    </script>
</body>
</html>