-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-11-2017 a las 22:26:11
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `dbrrhh`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbdivision`
--

CREATE TABLE IF NOT EXISTS `tbdivision` (
  `div_codigo` char(5) NOT NULL,
  `div_unicod` char(5) NOT NULL,
  `div_descri` varchar(200) NOT NULL,
  `div_estcod` int(11) NOT NULL,
  PRIMARY KEY (`div_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbdivision`
--

INSERT INTO `tbdivision` (`div_codigo`, `div_unicod`, `div_descri`, `div_estcod`) VALUES
('DIV01', 'UNI01', 'FACULTAD DE INVESTIGACION', 1),
('DIV02', 'UNI02', 'REDES Y COMUNICACIONES', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbunidad`
--

CREATE TABLE IF NOT EXISTS `tbunidad` (
  `uni_codigo` char(5) NOT NULL,
  `uni_descri` varchar(200) NOT NULL,
  `uni_codest` int(11) NOT NULL,
  PRIMARY KEY (`uni_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbunidad`
--

INSERT INTO `tbunidad` (`uni_codigo`, `uni_descri`, `uni_codest`) VALUES
('UNI01', 'GRUPO UTP', 1),
('UNI02', 'GRUPO IDAT', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbusuarios`
--

CREATE TABLE IF NOT EXISTS `tbusuarios` (
  `usu_codigo` varchar(8) NOT NULL,
  `usu_nombre` varchar(45) DEFAULT NULL,
  `usu_descri` varchar(45) DEFAULT NULL,
  `usu_passwd` varchar(45) DEFAULT NULL,
  `usu_email` varchar(45) DEFAULT NULL,
  `usu_imagen` text,
  `usu_estcod` int(11) DEFAULT NULL,
  PRIMARY KEY (`usu_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbusuarios`
--

INSERT INTO `tbusuarios` (`usu_codigo`, `usu_nombre`, `usu_descri`, `usu_passwd`, `usu_email`, `usu_imagen`, `usu_estcod`) VALUES
('11111111', 'CTORRES', 'CIELO TORRES', '123', '-', '-', 1),
('22222222', 'DTORRES', 'DEYSI TORRES', '456', '-', '-', 1),
('33333333', 'YTORRES', 'YUBARLENI TORRES', '456', '-', '-', 1),
('42455979', 'FTORRES', 'FERNANDO TORRES', '456', 'miandroidea@gmail.com', '-', 1),
('44444444', 'ETORRES', 'EMILIANO TORRES', '456', '-', '-', 1),
('55555555', 'ATORRES', 'ARLET TORRES', '456', '-', '-', 1),
('66666666', 'STORRES', 'ASTRID TORRES', '456', '-', '-', 1),
('77777777', 'NTORRES', 'NESTOR TORRES', '456', '-', '-', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
