<%-- 
    Document   : include_head
    Created on : 04/11/2017, 04:51:57 PM
    Author     : Bu
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>INSPINIA | Login 2</title>

    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="twitter-bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- <link href="font-awesome/css/font-awesome.css" rel="stylesheet"> -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- <link href="css/animate.css" rel="stylesheet"> -->
    
    <!-- Toastr style -->    
    <!-- <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet"> -->
    <link href="toastr.js/css/toastr.min.css" rel="stylesheet" type="text/css"/>
    
    <!-- FooTable -->
    <!-- <link href="css/plugins/footable/footable.core.css" rel="stylesheet"> -->
    <link href="jquery-footable/css/footable.core.min.css" rel="stylesheet" type="text/css"/>
    
    <!-- Gritter -->
    <link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    
    <!-- DatetimePicker-->
    <link href="jquery-datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    
    <link href="animate.css/animate.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/style.css" rel="stylesheet">
</head>
