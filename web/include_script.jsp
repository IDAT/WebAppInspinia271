<%-- 
    Document   : include_body
    Created on : 04/11/2017, 05:04:05 PM
    Author     : Bu
--%>

<!-- Mainly scripts -->
<script src="jquery/jquery.min.js" type="text/javascript"></script>
<!-- <script src="js/jquery-3.1.1.min.js"></script> -->
<script src="twitter-bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- <script src="js/bootstrap.min.js"></script> -->

<!-- <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script> -->
 <script src="metisMenu/metisMenu.min.js" type="text/javascript"></script>
<!-- <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script> -->
 <script src="jQuery-slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
 <!-- FooTable -->
 <!-- <script src="js/plugins/footable/footable.all.min.js"></script> -->
 <script src="jquery-footable/js/footable.all.min.js" type="text/javascript"></script>
 <!-- Flot -->
 <!-- <script src="js/plugins/flot/jquery.flot.js"></script> -->
 <script src="flot/jquery.flot.min.js" type="text/javascript"></script>
 <!-- <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script> -->
 <script src="flot.tooltip/jquery.flot.tooltip.min.js" type="text/javascript"></script>
 <script src="js/plugins/flot/jquery.flot.spline.js"></script>
 <!-- <script src="js/plugins/flot/jquery.flot.resize.js"></script> -->
 <script src="flot/jquery.flot.resize.min.js" type="text/javascript"></script>
 <!-- <script src="js/plugins/flot/jquery.flot.pie.js"></script> -->
 <script src="flot/jquery.flot.pie.min.js" type="text/javascript"></script>
 
 <!-- Peity -->
 <!-- <script src="js/plugins/peity/jquery.peity.min.js"></script> -->
 <script src="peity/jquery.peity.min.js" type="text/javascript"></script>
 <script src="js/demo/peity-demo.js"></script>
 
 <!-- Custom and plugin javascript -->
 <script src="js/inspinia.js"></script>
 <!-- <script src="js/plugins/pace/pace.min.js"></script> -->
 <script src="pace/pace.min.js" type="text/javascript"></script>
 <!-- jQuery UI -->
 <!-- <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script> -->
 <script src="jqueryui/jquery-ui.min.js" type="text/javascript"></script>

 <!-- GITTER -->
 <script src="js/plugins/gritter/jquery.gritter.min.js"></script>

 <!-- Sparkline -->
 <!-- <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script> -->
 <script src="jquery-sparklines/jquery.sparkline.min.js" type="text/javascript"></script>

 <!-- Sparkline demo data  -->
 <script src="js/demo/sparkline-demo.js"></script>

 <!-- ChartJS-->
 <!-- <script src="js/plugins/chartJs/Chart.min.js"></script> -->
 <script src="Chart.js/Chart.min.js" type="text/javascript"></script>
 <!-- Toastr -->
 <!-- <script src="js/plugins/toastr/toastr.min.js"></script> -->
 <script src="toastr.js/js/toastr.min.js" type="text/javascript"></script>
 
 <!-- Datetimepicker JS -->
 <script src="jquery-datetimepicker/jquery.datetimepicker.full.min.js" type="text/javascript"></script>



