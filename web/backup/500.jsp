<%-- 
    Document   : 500
    Created on : 04/11/2017, 05:01:39 PM
    Author     : Bu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
    <%@include file="include_head.jsp" %>
    
    <body class="gray-bg">

        <div class="middle-box text-center animated fadeInDown">
            <h1>500</h1>
            <h3 class="font-bold">Internal Server Error</h3>

            <div class="error-desc">
                The server encountered something unexpected that didn't allow it to complete the request. We apologize.<br/>
                You can go back to main page: <br/><a href="index.html" class="btn btn-primary m-t">Dashboard</a>
            </div>
        </div>
        <%@include file="include_body.jsp" %>
    </body>
</html>
