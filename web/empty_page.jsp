<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<!-- Style CSS -->
<%@include file="include_style.jsp" %>

<body class="">

    <div id="wrapper">
        <!-- NavBar -->
        <%@include file="include_navbar.jsp" %>    

        <div id="page-wrapper" class="gray-bg">
            <!-- NavBarHeader -->
            <%-- --%>
            <%@include file="include_navbarheader.jsp" %>            
            <%-- --%>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Titulo</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li class="active">
                            <strong>{Navegación Actual}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="" class="btn btn-primary">Botónes</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">              
                   
                    <!-- -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Body</h5>
                                </div>
                                <div class="ibox-content">
                                    <!-- TABLAS FORMS IU -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- -->
           
            </div>
            <!-- Footer -->
            <%--
            <%@include file="include_footer.jsp" %>
            --%>
        </div>
    </div>
    <!-- Script -->
    <%@include file="include_script.jsp" %>

</body>

</html>

