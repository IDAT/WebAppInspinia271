<%@page import="BusinessEntity.UsuarioModel"%>
<%@page import="BusinessLogic.UsuarioController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    UsuarioController UBL =new UsuarioController();
    UsuarioModel UBE = new UsuarioModel();
    //EVALUACION DE EXISTEN INICIAL DE PARAMETROS
    if(request.getParameter("accion") != null && request.getParameter("token") != null){        
        //ACCION -> ELIMINAR
        if("UPDATE".equalsIgnoreCase(request.getParameter("accion"))){            
            //TOKEN -> CODIGO USUARIO
            UBE = UBL.readUsuarioByCodigo(request.getParameter("token"));
        }        
    }
%>
<!DOCTYPE html>
<html>
    <%@include file="include_style.jsp" %>
<body>

    <div id="wrapper">
        <!-- NavBar -->
        <%@include file="include_navbar.jsp" %>    

        <div id="page-wrapper" class="gray-bg">
            <!-- NavBarHeader -->
            <%-- --%>
            <%@include file="include_navbarheader.jsp" %> 
            
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Usuarios</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Mantenimiento</a>
                        </li>
                        <li class="active">
                            <strong>Usuarios</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        
            
            <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
            
                <div class="col-md-10 col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Mantenimiento de Usuario</h5>                           
                        </div>
                        <div class="ibox-content">
                            <form method="post" action="rea_usuarios.jsp" class="form-horizontal">
                                <input type="hidden" name="accion" value="<%=request.getParameter("accion") %>">
                                <div class="form-group"><label class="col-sm-2 col-md-2 col-lg-2 control-label">Codigo</label>
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                        <input type="text" name="txtCodigo" placeholder="# DNI" class="form-control" value="<%=(UBE.getUsu_codigo()!= null ? UBE.getUsu_codigo():"")%>" maxlength="8" <%=(UBE.getUsu_codigo()!= null ? "readonly":"")%>>
                                    </div>
                                </div>
                                
                                <div class="form-group"><label class="col-sm-2 col-md-2 col-lg-2 control-label">Nombre</label>
                                    <div class="col-sm-10 col-md-10 col-lg-10 col-lg-10">
                                        <input type="text" name="txtNombre" placeholder="Nombre" class="form-control" value="<%=(UBE.getUsu_nombre()!= null ? UBE.getUsu_nombre():"")%>" maxlength="45">
                                    </div>
                                </div>
                                
                                <div class="form-group"><label class="col-sm-2 col-md-2 col-lg-2 control-label">Descripción</label>
                                    <div class="col-sm-10 col-md-10 col-lg-10 col-lg-10">
                                        <input type="text" name="txtDescri" placeholder="Descripción" class="form-control" value="<%=(UBE.getUsu_descri()!= null ? UBE.getUsu_descri():"")%>" maxlength="45">
                                    </div>
                                </div>
                                
                                <div class="form-group"><label class="col-sm-2 col-md-2 col-lg-2 control-label">Password</label>
                                    <div class="col-sm-10 col-md-10 col-lg-10 col-lg-10">
                                        <input type="password" name="txtPasswd" placeholder="Password" class="form-control" value="<%=(UBE.getUsu_passwd()!= null ? UBE.getUsu_passwd():"")%>" maxlength="45">
                                    </div>
                                </div> 
                                
                                <div class="form-group"><label class="col-sm-2 col-md-2 col-lg-2 control-label">Email</label>
                                    <div class="col-sm-10 col-md-10 col-lg-10 col-lg-10">
                                        <input type="email" name="txtEmail" placeholder="Email" class="form-control" value="<%=(UBE.getUsu_email()!= null ? UBE.getUsu_email():"")%>" maxlength="45">
                                    </div>
                                </div>
                                
                                <div class="form-group"><label class="col-sm-2 col-md-2 col-lg-2 control-label">Activo</label>
                                    <div class="col-sm-10 col-md-10 col-lg-10 col-lg-10">
                                        <select class="form-control m-b" name="cboEstado">
                                            <option value="-" >Seleccione</option>
                                            <option value="0" <%=( UBE.getUsu_estcod() == 0 ? "selected" : "") %>>INACTIVO</option>
                                            <option value="1" <%=( UBE.getUsu_estcod() == 1 ? "selected" : "") %>>ACTIVO</option>
                                        </select>
                                    </div>
                                </div>
                                                            
                                <div class="form-group">
                                    <div class="col-lg-10 text-center">
                                        <button class="btn btn-sm btn-primary" type="submit">Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
            
            <%@include file="include_footer.jsp" %>

        </div>
        </div>

   <%@include file="include_script.jsp" %>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {

            $('.footable').footable();
            $('.footable2').footable();

        });

    </script>

</body>

</html>