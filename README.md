## Proyecto WebAppInspinia271

Basado en Java JSP & ServLet con Base de datos MySQL en IDE NetBeans con Framework y Plugins JQuery + BootStrap + Footable:


1. Importar `export_database.sql` Ubicado en la carpeta `database`.
2. Aperture el proyecto con NetBeans IDE.

Desarrollado por : [superahacker]

## Servlet Usuario (UsuarioServlet.java)

Servicio ServLet preparado para trabajar metodos `doGET` `doPOST` `doPUT` `doDELETE`

### Metodo GET
`http://localhost:8080/Web/Usuario.do`

**Response:**
{
    "status": true,
    "message": "2 REGISTRO(S) ENCONTRADO(S)",
    "usuarios": [
        {
            "usu_codigo": "11111111",
            "usu_nombre": "CTORRES",
            "usu_descri": "CIELO TORRES",
            "usu_passwd": "123",
            "usu_email": "-",
            "usu_imagen": "-",
            "usu_estcod": 1
        },
        {
            "usu_codigo": "12345678",
            "usu_nombre": "AOCA�A",
            "usu_descri": "ASTRID OCA�A",
            "usu_passwd": "123",
            "usu_email": "-",
            "usu_imagen": "-",
            "usu_estcod": 1
        }
    ]
}

### Metodo POST
`http://localhost:8080/Web/Usuario.do`

**Request:**
{
    "usu_codigo": "99998888",
    "usu_nombre": "PBENDEZU",
    "usu_descri": "PAOLA BENDEZU",
    "usu_passwd": "456",
    "usu_email": "pbendezu@gmail.com",
    "usu_imagen": "-",
    "usu_estcod": 1
}

**Response:**
{
    "status": true,
    "message": "1 REGISTRO GUARDADO"
}

### Metodo PUT
`http://localhost:8080/Web/Usuario.do`

**Request:**
{
    "usu_codigo": "99998888",
    "usu_nombre": "ABENDEZU",
    "usu_descri": "ALEJANDRA BENDEZU",
    "usu_passwd": "456",
    "usu_email": "pbendezu@gmail.com",
    "usu_imagen": "-",
    "usu_estcod": 1
}

**Resonse:**
{
    "status": true,
    "message": "1 REGISTRO ACTUALIZADO"
}

### Metodo DELETE
`http://localhost:8080/Web/Usuario.do?codigo=99998888`

**Response:**
{
    "status": true,
    "message": "1 REGISTRO ELIMINADO"
}

[superahacker]: http://superahacker.blogspot.com/