package BusinessEntity;
public class DivisionModel {
    
    String div_codigo;
    String div_unicod;
    String div_descri;
    int div_estcod;

    public DivisionModel() {
    }

    public DivisionModel(String div_codigo, String div_unicod, String div_descri, int div_estcod) {
        this.div_codigo = div_codigo;
        this.div_unicod = div_unicod;
        this.div_descri = div_descri;
        this.div_estcod = div_estcod;
    }

    public String getDiv_codigo() {
        return div_codigo;
    }

    public void setDiv_codigo(String div_codigo) {
        this.div_codigo = div_codigo;
    }

    public String getDiv_unicod() {
        return div_unicod;
    }

    public void setDiv_unicod(String div_unicod) {
        this.div_unicod = div_unicod;
    }

    public String getDiv_descri() {
        return div_descri;
    }

    public void setDiv_descri(String div_descri) {
        this.div_descri = div_descri;
    }

    public int getDiv_estcod() {
        return div_estcod;
    }

    public void setDiv_estcod(int div_estcod) {
        this.div_estcod = div_estcod;
    }
        
}