package BusinessEntity;

public class DivisionBean extends DivisionModel{
   String uni_descri;
   
    public DivisionBean(){
    }
    public DivisionBean(String uni_descri, String div_codigo, String div_unicod, String div_descri, int div_estcod) {
        super(div_codigo, div_unicod, div_descri, div_estcod);
        this.uni_descri = uni_descri;
    }

    public String getUni_descri() {
        return uni_descri;
    }

    public void setUni_descri(String uni_descri) {
        this.uni_descri = uni_descri;
    }
   
   
}
