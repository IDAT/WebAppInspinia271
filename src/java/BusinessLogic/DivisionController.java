package BusinessLogic;

import BusinessEntity.DivisionBean;
import DataAccessObject.ConexionMySQL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class DivisionController extends ConexionMySQL {

    public static void main(String[] a) {
        DivisionController c = new DivisionController();
        ArrayList<DivisionBean> lst = c.reaDivision();      
        for (int x = 0; x < lst.size(); x++) {
            System.out.println(lst.get(x).getDiv_codigo());
        }

        DivisionBean b = c.readDivisionByCodigo("DIV02");
        System.out.println(b.getDiv_descri());
    }

    public ArrayList<DivisionBean> reaDivision() {
        ArrayList<DivisionBean> lst = null;
        try {
            StringBuilder SQL = new StringBuilder();
            SQL.append("SELECT d.div_codigo, d.div_unicod ,u.uni_descri, d.div_descri,d.div_estcod ");
            SQL.append("FROM tbdivision d ");
            SQL.append("INNER JOIN tbunidad u ON d.div_unicod = u.uni_codigo");
            
            Statement stm = null;
            ResultSet res = null;

            stm = getConexion().createStatement();
            res = stm.executeQuery(SQL.toString());
            lst = new ArrayList<>();
            while (res.next()) {
                DivisionBean i = new DivisionBean();
                i.setDiv_codigo(res.getString(1));
                i.setDiv_unicod(res.getString(2));
                i.setUni_descri(res.getString(3));
                i.setDiv_descri(res.getString(4));
                i.setDiv_estcod(res.getInt(5));
                lst.add(i);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lst;
    }

    public DivisionBean readDivisionByCodigo(String c) {
        DivisionBean b = new DivisionBean();
        try {
            String SQL = "SELECT * FROM tbdivision WHERE div_codigo = ? ";
            PreparedStatement pst = null;
            ResultSet res = null;
            pst = getConexion().prepareStatement(SQL);
            pst.setString(1, c);
            res = pst.executeQuery();
            while (res.next()) {
                b.setDiv_codigo(res.getString(1));
                b.setDiv_unicod(res.getString(2));
                b.setDiv_descri(res.getString(3));
                b.setDiv_estcod(res.getInt(4));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return b;

    }

}
